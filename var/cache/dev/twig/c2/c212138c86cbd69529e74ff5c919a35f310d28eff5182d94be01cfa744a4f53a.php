<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* todo_list/index.html.twig */
class __TwigTemplate_05f699d1af3fc522df86f7eec722afdc0b46523d90a41e30f1e6d180624fa6c2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "todo_list/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "todo_list/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "todo_list/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container d-flex justify-content-center\">
    <h1 > My todo list App</h1>
    
</div>  
<div class=\"container d-flex justify-content-center\">
    
    <a href=\"/create-list\" class=\"btn btn-success a btnradiusplus \"><img src=\"https://img.icons8.com/color/30/000000/add-list.png\"/>  Créer Vos listes </a></h4>
</div>    
        <div class=\"container d-flex flex-wrap \">
        
        ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["lists"]) || array_key_exists("lists", $context) ? $context["lists"] : (function () { throw new RuntimeError('Variable "lists" does not exist.', 14, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["list"]) {
            // line 15
            echo "            <div class=\"card  m-3 mb-3\" >
                    <div class=\"card-body \">
                    
                    <div class=\"container d-flex justify-content-center\">
                    
                    </div>
                        <h5 class=\"card-title\" style=\"color:";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["list"], "color", [], "any", false, false, false, 21), "html", null, true);
            echo "\">Voici votre liste : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["list"], "name", [], "any", false, false, false, 21), "html", null, true);
            echo "</h5>
                        <div class=\"container d-flex justify-content-center\">
                        <a href=\"/update-list/";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["list"], "id", [], "any", false, false, false, 23), "html", null, true);
            echo "\" class=\"btn btn-warning a btnradius \"> <img src=\"https://img.icons8.com/emoji/30/000000/pen-emoji.png\"/></i> Modifier </a>
                        </div>
                        <div class=\"container d-flex justify-content-center\">
                        <a href=\"/delete-list/";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["list"], "id", [], "any", false, false, false, 26), "html", null, true);
            echo "\" class=\"btn btn-danger btnradius a \"><i class=\"fas fa-trash-alt\"></i> Effacer </a><br> 
                        </div>
                        <h4 >Ajout de tâches</h4>
                        <div class=\"container d-flex justify-content-center\">
                        <a href=\"/create-task/";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["list"], "id", [], "any", false, false, false, 30), "html", null, true);
            echo "\" class=\"btn btn-info btnradius a \"><img src=\"https://img.icons8.com/dusk/30/000000/add-property.png\"/>  Ajouter </a>
                        </div>
                        <h4>Voici vos tâches :</h4>

                        ";
            // line 34
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["list"], "tasks", [], "any", false, false, false, 34));
            foreach ($context['_seq'] as $context["_key"] => $context["task"]) {
                // line 35
                echo "                                <p class=\"card-text\">
                                    
                                    <a href=\"/update-task-status/";
                // line 37
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 37), "html", null, true);
                echo "\" >
                                    ";
                // line 38
                if (twig_get_attribute($this->env, $this->source, $context["task"], "completed", [], "any", false, false, false, 38)) {
                    // line 39
                    echo "                                        
                                        <p class=\"textebarre texterouge\"><i class=\"fas fa-square  texterouge\"> </i> ";
                    // line 40
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "title", [], "any", false, false, false, 40), "html", null, true);
                    echo " <p> 
                                        <img src=\"https://img.icons8.com/doodle/50/000000/ranners-crossing-finish-line.png\"/> C'est terminer 
                                        <img src=\"https://img.icons8.com/doodle/48/000000/finish-flag.png\"/>
                                        </p>                                      
                                    ";
                } else {
                    // line 45
                    echo "                                        <i class=\"far fa-square\"> ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "title", [], "any", false, false, false, 45), "html", null, true);
                    echo "</i>
                                        
                                    ";
                }
                // line 47
                echo "  
                                    <div class=\"container d-flex justify-content-center\">
                                    <a href=\"/update-task/";
                // line 49
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 49), "html", null, true);
                echo "\" class=\"btn btn-warning btnradius a \"><i class=\"fas fa-user-edit\"></i> Modifier </a>
                                    </div>
                                    <div class=\"container d-flex justify-content-center\">
                                    <a href=\"/delete-task/";
                // line 52
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["task"], "id", [], "any", false, false, false, 52), "html", null, true);
                echo "\" class=\"btn btn-danger btnradius a \"><i class=\"fas fa-trash-alt\"></i> Effacer </a>
                                    </div>
                                     
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['task'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "                            
                            ";
            // line 57
            $context["counter"] = (((array_key_exists("counter", $context)) ? (_twig_default_filter((isset($context["counter"]) || array_key_exists("counter", $context) ? $context["counter"] : (function () { throw new RuntimeError('Variable "counter" does not exist.', 57, $this->source); })()), 0)) : (0)) + 1);
            // line 58
            echo "                                    <p class=\"compt\">
                                    <img src=\"https://img.icons8.com/external-inipagistudio-lineal-color-inipagistudio/40/000000/external-danger-neighborhood-watch-inipagistudio-lineal-color-inipagistudio.png\"/>
                                    Vous avez ";
            // line 60
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["list"], "tasks", [], "any", false, false, false, 60), "count", [], "any", false, false, false, 60), "html", null, true);
            echo "
                                    <img src=\"https://img.icons8.com/external-flatart-icons-flat-flatarticons/50/000000/external-task-work-from-home-flatart-icons-flat-flatarticons.png\"/>  
                                    ";
            // line 62
            echo "en cours à terminer";
            echo "
                                    </p>

                    </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['list'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 70
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 71
        echo "


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "todo_list/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 71,  209 => 70,  198 => 68,  186 => 62,  181 => 60,  177 => 58,  175 => 57,  172 => 56,  162 => 52,  156 => 49,  152 => 47,  145 => 45,  137 => 40,  134 => 39,  132 => 38,  128 => 37,  124 => 35,  120 => 34,  113 => 30,  106 => 26,  100 => 23,  93 => 21,  85 => 15,  81 => 14,  69 => 4,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends \"base.html.twig\" %}

{% block body %}
<div class=\"container d-flex justify-content-center\">
    <h1 > My todo list App</h1>
    
</div>  
<div class=\"container d-flex justify-content-center\">
    
    <a href=\"/create-list\" class=\"btn btn-success a btnradiusplus \"><img src=\"https://img.icons8.com/color/30/000000/add-list.png\"/>  Créer Vos listes </a></h4>
</div>    
        <div class=\"container d-flex flex-wrap \">
        
        {% for list in lists %}
            <div class=\"card  m-3 mb-3\" >
                    <div class=\"card-body \">
                    
                    <div class=\"container d-flex justify-content-center\">
                    
                    </div>
                        <h5 class=\"card-title\" style=\"color:{{list.color}}\">Voici votre liste : {{list.name}}</h5>
                        <div class=\"container d-flex justify-content-center\">
                        <a href=\"/update-list/{{list.id }}\" class=\"btn btn-warning a btnradius \"> <img src=\"https://img.icons8.com/emoji/30/000000/pen-emoji.png\"/></i> Modifier </a>
                        </div>
                        <div class=\"container d-flex justify-content-center\">
                        <a href=\"/delete-list/{{list.id }}\" class=\"btn btn-danger btnradius a \"><i class=\"fas fa-trash-alt\"></i> Effacer </a><br> 
                        </div>
                        <h4 >Ajout de tâches</h4>
                        <div class=\"container d-flex justify-content-center\">
                        <a href=\"/create-task/{{list.id }}\" class=\"btn btn-info btnradius a \"><img src=\"https://img.icons8.com/dusk/30/000000/add-property.png\"/>  Ajouter </a>
                        </div>
                        <h4>Voici vos tâches :</h4>

                        {% for task in list.tasks %}
                                <p class=\"card-text\">
                                    
                                    <a href=\"/update-task-status/{{task.id }}\" >
                                    {% if task.completed %}
                                        
                                        <p class=\"textebarre texterouge\"><i class=\"fas fa-square  texterouge\"> </i> {{task.title}} <p> 
                                        <img src=\"https://img.icons8.com/doodle/50/000000/ranners-crossing-finish-line.png\"/> C'est terminer 
                                        <img src=\"https://img.icons8.com/doodle/48/000000/finish-flag.png\"/>
                                        </p>                                      
                                    {% else %}
                                        <i class=\"far fa-square\"> {{task.title}}</i>
                                        
                                    {% endif %}  
                                    <div class=\"container d-flex justify-content-center\">
                                    <a href=\"/update-task/{{task.id }}\" class=\"btn btn-warning btnradius a \"><i class=\"fas fa-user-edit\"></i> Modifier </a>
                                    </div>
                                    <div class=\"container d-flex justify-content-center\">
                                    <a href=\"/delete-task/{{task.id }}\" class=\"btn btn-danger btnradius a \"><i class=\"fas fa-trash-alt\"></i> Effacer </a>
                                    </div>
                                     
                            {% endfor %}
                            
                            {% set counter = ( counter | default(0) ) + 1 %}
                                    <p class=\"compt\">
                                    <img src=\"https://img.icons8.com/external-inipagistudio-lineal-color-inipagistudio/40/000000/external-danger-neighborhood-watch-inipagistudio-lineal-color-inipagistudio.png\"/>
                                    Vous avez {{list.tasks.count }}
                                    <img src=\"https://img.icons8.com/external-flatart-icons-flat-flatarticons/50/000000/external-task-work-from-home-flatart-icons-flat-flatarticons.png\"/>  
                                    {{   'en cours à terminer'}}
                                    </p>

                    </div>
            </div>
        {% endfor %}
    </div>
{% endblock %}
{% block javascripts %}



{% endblock %}
    ", "todo_list/index.html.twig", "C:\\Users\\garod\\mytodo\\templates\\todo_list\\index.html.twig");
    }
}
