<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/create-list' => [[['_route' => 'create_list', '_controller' => 'App\\Controller\\TodoListController::create'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'read_all', '_controller' => 'App\\Controller\\TodoListController::readAll'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|wdt/([^/]++)(*:24)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:69)'
                            .'|router(*:82)'
                            .'|exception(?'
                                .'|(*:101)'
                                .'|\\.css(*:114)'
                            .')'
                        .')'
                        .'|(*:124)'
                    .')'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:159)'
                .')'
                .'|/create\\-task/([^/]++)(*:190)'
                .'|/update\\-(?'
                    .'|task(?'
                        .'|/([^/]++)(*:226)'
                        .'|\\-status/([^/]++)(*:251)'
                    .')'
                    .'|list/([^/]++)(*:273)'
                .')'
                .'|/delete\\-(?'
                    .'|task/([^/]++)(*:307)'
                    .'|list/([^/]++)(*:328)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        24 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        69 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        82 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        101 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        114 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        124 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        159 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        190 => [[['_route' => 'create_task', '_controller' => 'App\\Controller\\TaskController::create'], ['id'], null, null, false, true, null]],
        226 => [[['_route' => 'update_task', '_controller' => 'App\\Controller\\TaskController::update'], ['id'], null, null, false, true, null]],
        251 => [[['_route' => 'update_task_status', '_controller' => 'App\\Controller\\TaskController::updateTaskStatus'], ['id'], null, null, false, true, null]],
        273 => [[['_route' => 'update_list', '_controller' => 'App\\Controller\\TodoListController::update'], ['id'], null, null, false, true, null]],
        307 => [[['_route' => 'delete_task', '_controller' => 'App\\Controller\\TaskController::delete'], ['id'], null, null, false, true, null]],
        328 => [
            [['_route' => 'delete_list', '_controller' => 'App\\Controller\\TodoListController::delete'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
