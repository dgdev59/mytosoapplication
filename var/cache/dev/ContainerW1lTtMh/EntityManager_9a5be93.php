<?php

namespace ContainerW1lTtMh;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderf3195 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer2b0bd = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesdbaf6 = [
        
    ];

    public function getConnection()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getConnection', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getMetadataFactory', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getExpressionBuilder', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'beginTransaction', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getCache', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getCache();
    }

    public function transactional($func)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'transactional', array('func' => $func), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->transactional($func);
    }

    public function commit()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'commit', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->commit();
    }

    public function rollback()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'rollback', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getClassMetadata', array('className' => $className), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'createQuery', array('dql' => $dql), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'createNamedQuery', array('name' => $name), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'createQueryBuilder', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'flush', array('entity' => $entity), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'clear', array('entityName' => $entityName), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->clear($entityName);
    }

    public function close()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'close', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->close();
    }

    public function persist($entity)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'persist', array('entity' => $entity), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'remove', array('entity' => $entity), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'refresh', array('entity' => $entity), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'detach', array('entity' => $entity), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'merge', array('entity' => $entity), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getRepository', array('entityName' => $entityName), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'contains', array('entity' => $entity), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getEventManager', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getConfiguration', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'isOpen', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getUnitOfWork', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getProxyFactory', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'initializeObject', array('obj' => $obj), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'getFilters', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'isFiltersStateClean', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'hasFilters', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return $this->valueHolderf3195->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer2b0bd = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderf3195) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderf3195 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderf3195->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, '__get', ['name' => $name], $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        if (isset(self::$publicPropertiesdbaf6[$name])) {
            return $this->valueHolderf3195->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf3195;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf3195;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf3195;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf3195;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, '__isset', array('name' => $name), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf3195;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderf3195;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, '__unset', array('name' => $name), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf3195;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderf3195;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, '__clone', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        $this->valueHolderf3195 = clone $this->valueHolderf3195;
    }

    public function __sleep()
    {
        $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, '__sleep', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;

        return array('valueHolderf3195');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer2b0bd = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer2b0bd;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer2b0bd && ($this->initializer2b0bd->__invoke($valueHolderf3195, $this, 'initializeProxy', array(), $this->initializer2b0bd) || 1) && $this->valueHolderf3195 = $valueHolderf3195;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderf3195;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderf3195;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
